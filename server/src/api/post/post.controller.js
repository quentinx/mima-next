const request = require("../../../request");

exports.getLatestFive = async (req, res) => {
	const response = await request(
		"http://www.mimacaijing.net/api/?json=get_recent_posts&count=5"
	);

	return res.json(response.data);
};

exports.getCategory = async (req, res) => {
	const response = await request(
		`http://www.mimacaijing.net/api/?json=get_category_posts&count=15&page=${
			req.params.page
		}&slug=${encodeURI(req.params.category)}`
		// `http://www.mimacaijing.net/api/?json=get_category_posts&count=1000&slug=${encodeURI(
		// 	req.params.category
		// )}`
	);

	return res.json(response.data);
};

exports.getCategoryId = async (req, res) => {
	const response = await request(
		`http://www.mimacaijing.net/wp-json/wp/v2/categories?slug=${encodeURI(
			req.params.category
		)}`
	);

	return res.json(response.data);
};

exports.getCategories = async (req, res) => {
	const response = await request(
		"http://www.mimacaijing.net/api/?json=get_category_index"
	);

	return res.json(response.data);
};

exports.getAuthors = async (req, res) => {
	const response = await request(
		"http://www.mimacaijing.net/api/?json=get_author_index"
	);

	return res.json(response.data);
};

exports.getAuthor = async (req, res) => {
	const authors = await request(
		"http://www.mimacaijing.net/api/?json=get_author_index"
	);

	const id = authors.data.authors.find(
		(x) => x.name === decodeURI(req.params.authorName)
	).id;

	const response = await request(
		`http://www.mimacaijing.net/api/?json=get_author_posts&count=1000&id=${
			authors.data.authors.find(
				(x) => x.name === decodeURI(req.params.authorName)
			).id
		}&slug=${encodeURI("专栏")}`
	);

	return res.json(response.data);
};

exports.getPageviews = async (req, res) => {
	const response = await request(
		"http://app.mimacaijing.net/app/getPageviews/",
		{
			method: "POST",
			body: req.params.blogIds
		}
	);

	return res.json(response.data);
};

exports.getBlog = async (req, res) => {
	const response = await request(
		`http://www.mimacaijing.net/api/?json=get_post&id=${req.params.blogId}`
	);
	let next;
	let previous;
	if (response.data.next_url) {
		next = await request(
			`http://www.mimacaijing.net/api/?json=get_post&id=${
				response.data.next_url.match(/\d+/g).reverse()[0]
			}`
		);
	}
	if (response.data.previous_url) {
		previous = await request(
			`http://www.mimacaijing.net/api/?json=get_post&id=${
				response.data.previous_url.match(/\d+/g).reverse()[0]
			}`
		);
	}
	const related = await request(
		`http://www.mimacaijing.net/api/?json=get_category_posts&id=${
			response.data.post.categories[0].id
		}&count=3`
	);

	return res.json({
		post: response.data.post,
		next: next ? next.data.post : undefined,
		previous: previous ? previous.data.post : undefined,
		related: related.data.posts
	});
};

exports.getTag = async (req, res) => {
	const response = await request(
		`http://www.mimacaijing.net/api/get_tag_posts/?slug=${encodeURI(
			req.params.tag
		)}&&count=15&&page=${req.params.page}`
	);

	return res.json(response.data);
};

exports.getPageview = async (req, res) => {
	const response = await request(
		`http://app.mimacaijing.net/app/pageview/${req.params.blogId}/get`
	);

	return res.json(response.data);
};

exports.setPageview = async (req, res) => {
	const response = await request(
		`http://app.mimacaijing.net/app/pageview/${req.params.blogId}/set`
	);

	return res.json(response.data);
};

exports.setMetaAuthor = async (req, res) => {
	const authors = await request(
		"http://www.mimacaijing.net/api/?json=get_author_index"
	);

	const id = authors.data.authors.find(
		(x) => x.name === decodeURI(req.params.authorName)
	).id;

	const response = await request(
		`http://www.mimacaijing.net/wp-json/acf/v3/users/${id}`
	);

	return res.json(response.data.acf);
};

exports.setMetaBlog = async (req, res) => {
	const response = await request(
		`http://www.mimacaijing.net/wp-json/acf/v3/posts/${req.params.blogId}`
	);

	return res.json(response.data.acf);
};

exports.setMetaCategory = async (req, res) => {
	const response = await request(
		`http://www.mimacaijing.net/wp-json/acf/v3/categories/${
			req.params.categoryId
		}`
	);

	return res.json(response.data.acf);
};

exports.setMetaTag = async (req, res) => {
	const response = await request(
		`http://www.mimacaijing.net/wp-json/acf/v3/tags/${req.params.tagId}`
	);

	return res.json(response.data.acf);
};

exports.getCarousel = async (req, res) => {
	const response = await request(
		"http://www.mimacaijing.net/wp-json/wp/v2/pages?slug=banner"
	);
	const links = response.data[0].content.rendered.match(/<a(.*?)<\/a>/g);

	return res.json(links);
};

exports.getTags = async (req, res) => {
	const response = await request(
		"http://www.mimacaijing.net/api/get_tag_index/"
	);

	return res.json(response.data);
};
