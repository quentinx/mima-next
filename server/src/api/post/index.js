const express = require("express");
const Router = express.Router();

const postController = require("./post.controller");
const auth = require("../../authentication");

Router.get("/post/5", postController.getLatestFive);
Router.get("/category/:category/:page", postController.getCategory);
Router.get("/categoryId/:category", postController.getCategoryId);
Router.get("/categories", postController.getCategories);
Router.get("/authors", postController.getAuthors);
Router.get("/author/:authorName", postController.getAuthor);
Router.get("/pageviews/:blogIds", postController.getPageviews);
Router.get("/p/:blogId", postController.getBlog);
Router.get("/tag/:tag", postController.getTag);
Router.get("/tag/:tag/:page", postController.getTag);
Router.get("/pageview/:blogId/get", postController.getPageview);
Router.get("/pageview/:blogId/set", postController.setPageview);
Router.get("/meta/blog/:blogId/", postController.setMetaBlog);
Router.get("/meta/categories/:categoryId/", postController.setMetaCategory);
Router.get("/meta/tags/:tagId/", postController.setMetaTag);
Router.get("/meta/author/:authorName/", postController.setMetaAuthor);
Router.get("/carousel", postController.getCarousel);
Router.get("/tags", postController.getTags);

module.exports = Router;
