require("dotenv").load();
const express = require("express");
const next = require("next");
const http = require("http");
const cookieParser = require("cookie-parser");
const compression = require("compression");
const bodyParser = require("body-parser");
const expressValidator = require("express-validator");
const logger = require("morgan");
const chalk = require("chalk");
const mongoose = require("mongoose");
const routes = require("../../router");

const dev = process.env.NODE_ENV !== "production";

const app = next({ dev });
const handle = app.getRequestHandler();
// const handle = routes.getRequestHandler(app);
// Export for controllers
module.exports = {
	app,
	handle
};

const morganMode = dev ? "dev" : "common";
const mongodbURI = dev ? "mongodb://localhost:27017/boilerplate" : "remoteURI";

const controller = require("./global-controller");

/**
 * Routers
 */
// const userRouter = require('./api/users')
const postRouter = require("./api/post");

app
	.prepare()
	.then(() => {
		const server = express();

		// mongoose.Promise = global.Promise;
		// mongoose
		// 	.connect(mongodbURI)
		// 	.then(() =>
		// 		console.log(
		// 			`%s Connected to ${dev ? "localDB" : "remoteDB"}`,
		// 			chalk.green("✓")
		// 		)
		// 	)
		// 	.catch(err => {
		// 		console.log(err);
		// 		console.log(
		// 			"%s MongoDB connection error! Please make sure that MongoDB is running",
		// 			chalk.red("✗")
		// 		);
		// 	});

		server.set("port", process.env.PORT || 6678);
		server.use(cookieParser());
		server.use(compression());
		server.use(logger(morganMode));
		server.use(bodyParser.json());
		server.use(bodyParser.urlencoded({ extended: true }));
		server.use(expressValidator());
		// server.use(handle);

		// server.use('/api/users', userRouter)
		server.use("/api", postRouter);

		server.get("/robots.txt", (req, res) =>
			res.status(200).sendFile("robots.txt", {
				root: __dirname + "/static/",
				headers: {
					"Content-Type": "text/plain;charset=UTF-8"
				}
			})
		);
		server.get("/sitemap.xml", (req, res) =>
			res.status(200).sendFile("sitemap.xml", {
				root: __dirname + "/static/",
				headers: {
					"Content-Type": "text/plain;charset=UTF-8"
				}
			})
		);
		server.get("/favicon.png", (req, res) =>
			res.status(200).sendFile("favicon.png", {
				root: __dirname + "/static/"
			})
		);
		server.get("/nmfJS0AVjnUhENLbHCZbIsKYTXim5j.html", (req, res) =>
			res.status(200).sendFile("nmfJS0AVjnUhENLbHCZbIsKYTXim5j.html", {
				root: __dirname + "/static/"
			})
		);

		server.get("*", controller.handleNormalRequest);

		server.listen(server.get("port"), err => {
			if (err) throw err;
			console.log(
				"|> !!!Server is running on http://localhost:%s",
				server.get("port")
			);
		});
	})
	.catch(ex => {
		console.log(ex.stack);
		process.exit(1);
	});
