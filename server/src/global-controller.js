const next = require("next");
const { parse } = require("url");
const { app, handle } = require("./server");

exports.handleNormalRequest = (req, res) => {
	const parsedUrl = parse(req.url, true);
	const { pathname, query } = parsedUrl;
	if (pathname.startsWith("/authors/")) {
		return app.render(req, res, "/authors", {
			// authors: pathname.replace("/authors/", "")
		});
	} else if (pathname.startsWith("/@")) {
		return app.render(req, res, "/author", {
			authorName: pathname.match(/@(.*)/)[1]
		});
	} else if (pathname.startsWith("/s/")) {
		if (pathname.match(/\/s\/(.*)\/(.*)/)) {
			return app.render(req, res, "/sub_blogs", {
				category: pathname.match(/\/s\/(.*)\/(.*)/)[2]
			});
		} else {
			return app.render(req, res, "/blogs", {
				category: pathname.replace("/s/", "")
			});
		}
	} else if (pathname.startsWith("/tag")) {
		if (pathname.match(/\/tag\/(.*)\/(.*)/)) {
			return app.render(req, res, "/tag", {
				tag: pathname.match(/\/tag\/(.*)\/(.*)/)[1]
			});
		} else if (pathname.match(/\/tag\/(.*)-(.*)/)) {
			return app.render(req, res, "/p", {
				blogId: pathname.match(/\/tag\/(.*)-(.*)/)[2]
			});
		} else {
			return app.render(req, res, "/tag", {
				tag: pathname.replace("/tag/", "")
			});
		}
	} else if (pathname.startsWith("/p")) {
		return app.render(req, res, "/p", {
			blogId: pathname.replace("/p/", "")
		});
	}

	return handle(req, res, parsedUrl);
};

exports.handleNextRequest = (req, res) => {
	const pathname = req.route.path;
	const splittedPathname = pathname.split("/");
	const pathList = splittedPathname.filter(
		ele => ele.length > 0 && ele[0] !== ":"
	);
	const path = "/".concat(pathList.join("/"));

	return app.render(req, res, path, req.params);
};
