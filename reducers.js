import { combineReducers } from "redux"

// import authReducer from "src/auth/reducer"
import postReducer from "src/post/reducer"

export default combineReducers({
  // auth: authReducer,
  post: postReducer
})