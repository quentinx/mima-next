import { Row } from "antd";
import { Component } from "react";
import BasicContainer from "./BasicContainer";

const BlogViewContainer = ChildComponent => {
	class HigherOrderComponent extends Component {
		static async getInitialProps(ctx) {
			// Do something in serverside here
			const childProps = ChildComponent.getInitialProps
				? await ChildComponent.getInitialProps(ctx)
				: {};

			return { ...childProps };
		}

		render() {
			return (
				<BasicContainer>
					<Row type="flex" span={24}>
						<ChildComponent {...this.props} />
					</Row>
				</BasicContainer>
			);
		}
	}

	return HigherOrderComponent;
};

export default BlogViewContainer;
