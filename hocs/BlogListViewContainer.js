import { Component } from "react";
import BasicContainer from "./BasicContainer";

const BlogListViewContainer = ChildComponent => {
	class HigherOrderComponent extends Component {
		static async getInitialProps(ctx) {
			// Do something in serverside here
			const childProps = ChildComponent.getInitialProps
				? await ChildComponent.getInitialProps(ctx)
				: {};

			return { ...childProps };
		}

		render() {
			return (
				<BasicContainer>
					<ChildComponent {...this.props} />
				</BasicContainer>
			);
		}
	}

	return HigherOrderComponent;
};

export default BlogListViewContainer;
