import { Component } from "react";
import AuthorListContainer from "./AuthorListContainer";

const AuthorViewContainer = ChildComponent => {
	class HigherOrderComponent extends Component {
		static async getInitialProps(ctx) {
			// Do something in serverside here
			const childProps = ChildComponent.getInitialProps
				? await ChildComponent.getInitialProps(ctx)
				: {};

			return { ...childProps };
		}

		render() {
			return (
				<AuthorListContainer>
					<ChildComponent {...this.props} />
				</AuthorListContainer>
			);
		}
	}

	return HigherOrderComponent;
};

export default AuthorViewContainer;
