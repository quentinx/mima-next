import { LocaleProvider } from "antd";
import vi from "antd/lib/locale-provider/en_US";
import Head from "next/head";
import { Fragment } from "react";
import { connect } from "react-redux";

class RootContainer extends React.Component {
	render() {
		let title, keywords, description;
		if (typeof this.props.meta === "undefined") {
			title = "密码财经";
			keywords = "";
			description = "";
		} else {
			title = this.props.meta.data.title;
			description = this.props.meta.data.description;
			keywords = this.props.meta.data.keywords;
		}
		return (
			<Fragment>
				<Head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					<meta http-equiv="Resource-Type" content="document" />
					<meta http-equiv="Content-Script-Type" content="text/javascript" />
					<meta http-equiv="Content-Style-Type" content="text/css" />
					<meta httpEquiv="x-ua-compatible" content="ie=edge" />
					<meta name="viewport" content="width=device-width, initial-scale=1" />
					<meta
						name="google-site-verification"
						content="JNDka0OHTa1cS1nSOjNwXqZuAo2Ue5IZVVM4CIZLIiQ"
					/>
					<meta name="baidu-site-verification" content="cIIE3oKpi2" />
					<meta name="robots" content="index, follow" />
					<meta name="googlebot" content="index, follow" />
					<meta name="revisit-after" content="1 days" />
					<meta name="language" content="Chinese" />
					<meta name="copyright" content="广州密码传媒有限公司" />
					<meta name="contactOrganization" CONTENT="广州密码传媒有限公司" />
					<meta name="contactZipcode" CONTENT="510000" />
					<meta name="contactCity" CONTENT="Guangzhou" />
					<meta name="contactCountry" CONTENT="China" />
					<meta name="contactNetworkAddress" CONTENT="bd@mimacaijing.com" />
					<meta name="rating" content="general" />
					<meta name="reply-to" content="bd@mimacaijing.com" />
					<meta name="web_author" content="Quentin Gan Hongzhao" />
					<meta name="operator" CONTENT="Quentin Gan Hongzhao" />
					<meta http-equiv="Cache-control" content="public" />
					<title>{title}</title>
					<meta name="keywords" content={keywords} />
					<meta name="news_keywords" content={keywords} />
					<meta name="description" content={description} />
					<meta name="abstract" content={description} />
					<link rel="canonical" href="http://www.mimacaijing.com/" />
					<link href="/static/css/antd.min.css" rel="stylesheet" />
					<link href="/static/dist/main.css" rel="stylesheet" />
					<link
						rel="shortcut icon"
						type="image/png"
						href="/static/image/favicon.png"
					/>
					<script src="https://hm.baidu.com/hm.js?6c1ca476d3d873463914c438c059388e" />
				</Head>
				<LocaleProvider locale={vi}>{this.props.children}</LocaleProvider>
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		meta: state.post.meta
	};
};

const mapDispatchToProps = dispatch => {
	return {};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RootContainer);
