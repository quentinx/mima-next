import MainLayout from "layouts/main.layout";
import RootContainer from "./RootContainer";


const BasicContainer = props => (
	<RootContainer>
		<MainLayout>{props.children}</MainLayout>
	</RootContainer>
);

export default BasicContainer;
