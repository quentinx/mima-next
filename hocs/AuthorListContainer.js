import AuthorListLayout from "layouts/authorList.layout";
import RootContainer from "./RootContainer";

const BasicContainer = props => (
	<RootContainer>
		<AuthorListLayout>{props.children}</AuthorListLayout>
	</RootContainer>
);

export default BasicContainer;
