const routes = require("next-routes");

module.exports = routes()
	// .add("about")
	// .add("blogs", "/blogs/:category")
	// .add("user", "/user/:id", "profile")
	// .add("/:noname/:lang(en|es)/:wow+", "complex")
	// .add({ name: "beta", pattern: "/v3", page: "v3" })
	// .add({ name: "blogs", pattern: "/blogs/:category", page: "blogs" })
	// .add({ name: "posts", pattern: "/posts/:number", page: "blogs" })
	// .add({ name: "index", pattern: "/", page: "index" })
