import request from "../../utils/request";

export const getLatestFive = () => {
	return function(dispatch) {
		return request("http://localhost:6678/api/post/5").then((response) => {
			dispatch({
				type: "POST.GET_LATEST_FIVE_SUCCESS",
				payload: response
			});
		});
	};
};

export const setMetaCategories = (categoryId) => {
	return function(dispatch) {
		return request(
			`http://localhost:6678/api/meta/categories/${categoryId}`
		).then((response) => {
			dispatch({
				type: "POST.SET_META_SUCCESS",
				payload: response
			});
		});
	};
};

export const getCategoryPosts = (category, page, isIndex) => {
	return function(dispatch) {
		dispatch({
			type: "POST.GET_CATEGORY_POSTS",
			payload: true
		});

		return request(
			`http://localhost:6678/api/category/${encodeURI(category)}/${page}`
		).then(async (response) => {
			await request(
				isIndex
					? `http://localhost:6678/api/meta/categories/173/`
					: `http://localhost:6678/api/meta/categories/${
							response.data.category.id
					  }`
			).then((response) => {
				dispatch({
					type: "POST.SET_META_SUCCESS",
					payload: response
				});
			});
			dispatch({
				type: "POST.GET_CATEGORY_POSTS_SUCCESS",
				payload: response
			});
		});
	};
};

export const getMimaVideo = (category = "密码情报站", page = 1) => {
	return function(dispatch) {
		dispatch({
			type: "POST.GET_MIMA_VIDEO",
			payload: true
		});
		return request(
			`http://localhost:6678/api/category/${encodeURI(category)}/${page}`
		).then((response) => {
			dispatch({
				type: "POST.GET_MIMA_VIDEO_SUCCESS",
				payload: response
			});
		});
	};
};

export const getMimaVideo2 = (category = "密码专访", page = 1) => {
	return function(dispatch) {
		dispatch({
			type: "POST.GET_MIMA_2_VIDEO",
			payload: true
		});
		return request(
			`http://localhost:6678/api/category/${encodeURI(category)}/${page}`
		).then((response) => {
			dispatch({
				type: "POST.GET_MIMA_VIDEO_2_SUCCESS",
				payload: response
			});
		});
	};
};

export const getMoreCategoryPosts = (category, page) => {
	return function(dispatch) {
		dispatch({
			type: "POST.GET_CATEGORY_POSTS",
			payload: true
		});
		return request(
			`http://www.mimacaijing.com/api/category/${encodeURI(category)}/${page}`
		).then((response) => {
			dispatch({
				type: "POST.GET_MORE_CATEGORY_POSTS_SUCCESS",
				payload: response
			});
		});
	};
};

export const getCategories = () => {
	return function(dispatch) {
		return request(`http://localhost:6678/api/categories`).then((response) => {
			dispatch({
				type: "POST.GET_CATEGORIES_SUCCESS",
				payload: response
			});
		});
	};
};

export const getAuthors = () => {
	return async function(dispatch) {
		return await request(`http://localhost:6678/api/authors`).then(
			async (response) => {
				const authorNames = response.data.authors
					.filter((x) => x.slug.startsWith("zhuanlan"))
					.map((x) => x.name);
				const final = await Promise.all(
					authorNames.map((x) =>
						request(`http://localhost:6678/api/author/${encodeURI(x)}`)
					)
				)
					.then(async (results) => {
						const blogIds = results
							.map((result) => result.data.posts.map((p) => p.id))
							.map((x) => x.toString());
						let finalResult = results.map((r) => {
							return {
								count: r.data.count,
								author: r.data.author
							};
						});
						await Promise.all(
							blogIds.map((x) =>
								request(`http://localhost:6678/api/pageviews/${x}`)
							)
						)
							.then((pageviews) => {
								finalResult.forEach(
									(f, i) => (f["pageviews"] = pageviews[i].data.pageviews)
								);
							})
							.catch((error) => console.error(error));
						return finalResult;
					})
					.catch((error) => console.error(error));
				dispatch({
					type: "POST.GET_AUTHORS_SUCCESS",
					payload: final
				});
			}
		);
	};
};

export const getAuthor = (authorName) => {
	return function(dispatch) {
		request(`http://localhost:6678/api/meta/author/${authorName}`).then(
			(response) => {
				dispatch({
					type: "POST.SET_META_SUCCESS",
					payload: response
				});
			}
		);
		return request(
			`http://localhost:6678/api/author/${encodeURI(authorName)}`
		).then(async (response) => {
			const { author, count, posts } = response.data;
			const { url, description, name } = author;
			const blogIds = posts.map((p) => p.id).toString();
			const pageviews = await request(
				`http://localhost:6678/api/pageviews/${blogIds}`
			).then((v) => v.data.pageviews);

			dispatch({
				type: "POST.GET_AUTHOR_SUCCESS",
				payload: {
					url,
					name,
					description,
					count,
					pageviews,
					posts
				}
			});
		});
	};
};

export const getPageviews = (postids) => {
	return function(dispatch) {
		return request(`http://localhost:6678/api/pageviews`, {
			method: "POST",
			body: postids.toString()
		}).then((response) => {
			dispatch({
				type: "POST.GET_PAGEVIEWS_SUCCESS",
				payload: response
			});
		});
	};
};

export const getBlog = (blogId) => {
	return function(dispatch) {
		request(`http://localhost:6678/api/meta/blog/${blogId}`).then(
			(response) => {
				dispatch({
					type: "POST.SET_META_SUCCESS",
					payload: response
				});
			}
		);
		return request(`http://localhost:6678/api/p/${blogId}`).then((response) => {
			dispatch({
				type: "POST.GET_BLOG_SUCCESS",
				payload: response
			});
		});
	};
};

export const getPageview = (blogId) => {
	return function(dispatch) {
		return request(
			`http://localhost:6678/api/pageview/${encodeURI(blogId)}/get`
		).then((response) => {
			dispatch({
				type: "POST.GET_PAGEVIEW_SUCCESS",
				payload: response
			});
		});
	};
};

export const setPageview = (blogId) => {
	return function(dispatch) {
		return request(
			`http://localhost:6678/api/pageview/${encodeURI(blogId)}/set`
		).then((response) => {
			dispatch({
				type: "POST.SET_PAGEVIEW_SUCCESS",
				payload: response
			});
		});
	};
};

export const getTag = (tag) => {
	return function(dispatch) {
		return request(`http://localhost:6678/api/tag/${encodeURI(tag)}`).then(
			async (response) => {
				await request(
					`http://localhost:6678/api/meta/tags/${response.data.tag.id}`
				).then((response) => {
					dispatch({
						type: "POST.SET_META_SUCCESS",
						payload: response
					});
				});
				dispatch({
					type: "POST.GET_TAG_SUCCESS",
					payload: response
				});
			}
		);
	};
};

export const getMoreTag = (tag, page) => {
	return function(dispatch) {
		dispatch({
			type: "POST.GET_TAG",
			payload: true
		});
		return request(`http://www.mimacaijing.com/api/tag/${tag}/${page}`).then(
			(response) => {
				dispatch({
					type: "POST.GET_MORE_TAG_SUCCESS",
					payload: response
				});
			}
		);
	};
};

export const getDataSource = () => {
	return function(dispatch) {
		return request("http://app.mimacaijing.net/data/").then((response) => {
			dispatch({
				type: "POST.GET_DATA_SOURCE_SUCCESS",
				payload: response
			});
		});
	};
};

export const getCarousel = () => {
	return function(dispatch) {
		return request("http://localhost:6678/api/carousel").then((response) => {
			dispatch({
				type: "POST.GET_CAROUSEL_SUCCESS",
				payload: response
			});
		});
	};
};

export const getTags = () => {
	return function(dispatch) {
		return request("http://localhost:6678/api/tags").then((response) => {
			dispatch({
				type: "POST.GET_TAGS_SUCCESS",
				payload: response
			});
		});
	};
};

export const getPostsByCategory = (category) => {
	return function(dispatch) {
		return request(
			`http://localhost:6678/api/category/${encodeURI(category)}/1/`
		).then(async (response) => {
			dispatch({
				type: "POST.GET_POSTS_BY_Category_SUCCESS",
				payload: response
			});
		});
	};
};
