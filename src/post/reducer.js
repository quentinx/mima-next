import update from "immutability-helper";

const initState = {
	latestFive: null,
	category: {
		loading: false
	},
	categories: null,
	authors: null,
	author: null,
	blog: null,
	related: null,
	pageview: null, // ???
	pageviews: null // ???
};

export default (state = initState, action) => {
	switch (action.type) {
		case "POST.GET_LATEST_FIVE_SUCCESS":
			return update(state, {
				latestFive: { $set: action.payload },
				loading: { $set: false }
			});
		case "POST.GET_CATEGORY_POSTS":
			return update(state, { loading: { $set: action.payload } });
		case "POST.GET_CATEGORY_POSTS_SUCCESS":
			return update(state, {
				category: { $set: action.payload },
				loading: { $set: false }
			});

		case "POST.GET_MIMA_VIDEO":
			return update(state, { loading: { $set: action.payload } });
		case "POST.GET_MIMA_VIDEO_SUCCESS":
			return update(state, {
				qingbaozhan: { $set: action.payload },
				loading: { $set: false }
			});
		case "POST.GET_MIMA_VIDEO_2":
			return update(state, { loading: { $set: action.payload } });
		case "POST.GET_MIMA_VIDEO_2_SUCCESS":
			return update(state, {
				zhuanfang: { $set: action.payload },
				loading: { $set: false }
			});
		case "POST.GET_MORE_CATEGORY_POSTS_SUCCESS":
			return update(state, {
				category: {
					data: {
						posts: { $push: action.payload.data.posts }
					}
				},
				loading: { $set: false }
			});
		case "POST.GET_CATEGORIES_SUCCESS":
			return update(state, {
				categories: { $set: action.payload },
				loading: { $set: false }
			});
		case "POST.GET_AUTHORS_SUCCESS":
			return update(state, {
				authors: { $set: action.payload },
				loading: { $set: false }
			});
		case "POST.GET_AUTHOR_SUCCESS":
			return update(state, {
				author: { $set: action.payload },
				loading: { $set: false }
			});
		case "POST.GET_PAGEVIEWS_SUCCESS":
			// ？？？
			return update(state, { pageviews: { $set: action.payload } });
		case "POST.GET_BLOG_SUCCESS":
			return update(state, {
				blog: { $set: action.payload },
				loading: { $set: false }
			});
		case "POST.GET_RELATED_SUCCESS":
			return update(state, { related: { $set: action.payload } });
		case "POST.GET_PAGEVIEW_SUCCESS":
			// ？？？
			return update(state, { pageview: { $set: action.payload } });
		case "POST.SET_PAGEVIEW_SUCCESS":
			// ？？？
			return update(state, { pageview: { $set: action.payload } });
		case "POST.SET_META_SUCCESS":
			return update(state, { meta: { $set: action.payload } });
		case "POST.GET_TAG":
			return update(state, { loading: { $set: action.payload } });
		case "POST.GET_TAG_SUCCESS":
			return update(state, {
				tag: { $set: action.payload },
				loading: { $set: false }
			});
		case "POST.GET_TAGS_SUCCESS":
			return update(state, {
				tags: { $set: action.payload.data.tags }
			});
		case "POST.GET_MORE_TAG_SUCCESS":
			return update(state, {
				tag: {
					data: {
						posts: { $push: action.payload.data.posts }
					}
				},
				loading: { $set: false }
			});
		case "POST.GET_DATA_SOURCE_SUCCESS":
			return update(state, {
				dataSource: {
					$set: action.payload
				}
			});
		case "POST.GET_CAROUSEL_SUCCESS":
			return update(state, {
				carousel: {
					$set: action.payload
				}
			});
		case "POST.GET_POSTS_BY_Category_SUCCESS":
			return update(state, {
				postsByCategory: {
					$set: action.payload
				}
			});
		default:
			return state;
	}
};
