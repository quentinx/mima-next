import { Col, Menu, Row } from "antd";
import Link from "next/link";
import React from "react";
import header_sprite from "/static/image/header_sprite.png";
import { SpriteSheet } from "react-spritesheet";
import logo from "/static/image/logo.svg";

export default class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			weibo: false,
			toutiao: false,
			wechat: false
		};
	}

	render() {
		const spriteSheet = {
			weibo: {
				x: 0,
				y: 0,
				width: 40,
				height: 24
			},
			toutiao: {
				x: 39,
				y: 0,
				width: 40,
				height: 24
			},
			wechat: {
				x: 73,
				y: 0,
				width: 40,
				height: 24
			},
			weibo_active: {
				x: 0,
				y: 33,
				width: 40,
				height: 24
			},
			toutiao_active: {
				x: 39,
				y: 33,
				width: 40,
				height: 24
			},
			wechat_active: {
				x: 73,
				y: 33,
				width: 40,
				height: 24
			}
		};
		// let slug = this.props.location.pathname ? this.props.location.pathname : "";
		// slug = slug ? slug : "index";

		return (
			<Row
				type="flex"
				justify="center"
				className={"header"}
				style={{ backgroundColor: "white", boxShadow: "0px 0px 6px 0px" }}
			>
				<Row
					type="flex"
					justify="space-between"
					align="middle"
					style={{ width: 1200 }}
				>
					<Col>
						<Row type="flex" align="middle">
							<Col>
								<Link href="/">
									<a target="_blank">
										<img src={logo} alt="密码财经" width={180} />
									</a>
								</Link>
							</Col>

							<Col
								style={{ marginLeft: 70, fontSize: 16 }}
								className={"header_links"}
							>
								{/* <Menu selectedKeys={[slug]} mode="horizontal">
									{menuItems}
								</Menu> */}
								<Row type="flex" justify="space-between" gutter={20}>
									<Col>
										<a href="/">首页</a>
									</Col>
									<Col>
										<a href="/authors">专栏</a>
									</Col>
									<Col>
										<a href={`/s/${decodeURI("密码百科")}`}>密码百科</a>
									</Col>
									<Col>
										<a href={`/s/${decodeURI("DApp")}`}>DApp</a>
									</Col>
									<Col>
										<a href={`/s/${decodeURI("密码之声")}`}>密码之声</a>
									</Col>
									<Col>
										<a href={`/s/${decodeURI("视频")}`}>视频</a>
									</Col>
									<Col>
										<a href={"/market"}>行情</a>
									</Col>
								</Row>
							</Col>
						</Row>
					</Col>
					<Col>
						<Row type="flex">
							<div
								onMouseEnter={() => {
									this.setState({ weibo: true });
								}}
								onMouseLeave={() => {
									this.setState({ weibo: false });
								}}
							>
								{this.state.weibo ? (
									<div>
										<a
											target="_blank"
											rel="noopener noreferrer"
											href="https://weibo.com/5955571468"
										>
											<SpriteSheet
												filename={header_sprite}
												data={spriteSheet}
												sprite="weibo_active"
											/>
										</a>
									</div>
								) : (
									<SpriteSheet
										filename={header_sprite}
										data={spriteSheet}
										sprite="weibo"
									/>
								)}
							</div>
							<div
								onMouseEnter={() => {
									this.setState({ toutiao: true });
								}}
								onMouseLeave={() => {
									this.setState({ toutiao: false });
								}}
							>
								{this.state.toutiao ? (
									<a
										target="_blank"
										rel="noopener noreferrer"
										href="https://www.toutiao.com/c/user/99019067536/#mid=1601691120647181"
									>
										<SpriteSheet
											filename={header_sprite}
											data={spriteSheet}
											sprite="toutiao_active"
										/>
									</a>
								) : (
									<SpriteSheet
										filename={header_sprite}
										data={spriteSheet}
										sprite="toutiao"
									/>
								)}
							</div>
							<div
								onMouseEnter={() => {
									this.setState({ wechat: true });
								}}
								onMouseLeave={() => {
									this.setState({ wechat: false });
								}}
							>
								{this.state.wechat ? (
									<div style={{ position: "relative" }}>
										<SpriteSheet
											filename={header_sprite}
											data={spriteSheet}
											sprite="wechat_active"
										/>
										<div
											style={{
												position: "absolute",
												top: 47,
												right: 0,
												width: 150,
												height: 150,
												zIndex: 1
											}}
										>
											<img
												src={require("/static/image/wechat.png")}
												alt="wechat"
												style={{ width: 150 }}
											/>
										</div>
									</div>
								) : (
									<SpriteSheet
										filename={header_sprite}
										data={spriteSheet}
										sprite="wechat"
									/>
								)}
							</div>
						</Row>
					</Col>
				</Row>
			</Row>
		);
	}
}
