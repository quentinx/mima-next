import { Card, Carousel, Col, Row } from "antd";
import { Component } from "react";
import { connect } from "react-redux";
import MainLayout from "./main.layout";

class ContentLayout extends Component {
	render() {
		return (
			<MainLayout>
				<Row type="flex" span={24}>
					<Col span={16}>
						<Carousel>
							<img src="../static/image/banner.jpg" />
							<img src="../static/image/banner.jpg" />
							<img src="../static/image/banner.jpg" />
						</Carousel>
						<Card>{this.props.children}</Card>
					</Col>
					<Col span={8}>2</Col>
				</Row>
			</MainLayout>
		);
	}
}

const mapStateToProps = state => {
	return {
		auth: state.auth
	};
};

export default connect(
	mapStateToProps,
	{  }
)(ContentLayout);
