import React from "react";
import { Row, Col, Card, Divider } from "antd";
import Link from "next/link";
const Footer = () => {
	return (
		<Row
			className="footer"
			style={{
				backgroundColor: "#272a31",
				marginTop: 355,
				paddingTop: 40
			}}
		>
			<Col
				style={{
					display: "flex",
					flexDirection: "column"
				}}
			>
				<Row
					type="flex"
					justify="space-between"
					style={{ minWidth: 1200, flexDirection: "row", alignSelf: "center" }}
				>
					<Col>
						<Card title="关于" bordered={false}>
							<Row>
								<Link href={"/about"}>
									<a target="_blank" rel="noopener noreferrer">
										关于我们
									</a>
								</Link>
							</Row>
							<Row sytle={{ color: "##ff9b2b" }}>
								商务合作：bd@mimacaijing.com
							</Row>
						</Card>
					</Col>
					<Col>
						<Card title="密码财经公众号" bordered={false}>
							<img
								src={"/static/image/wechat.png"}
								alt="密码财经公众号"
								style={{ width: 150, height: 150 }}
							/>
						</Card>
					</Col>
					<Col>
						<Card title="密码财经头条" bordered={false}>
							<img
								src={"/static/image/toutiao.png"}
								alt="密码财经头条"
								style={{ width: 150, height: 150 }}
							/>
						</Card>
					</Col>
					<Col>
						<Card title="密码财经微博" bordered={false}>
							<img
								src={"/static/image/weibo.png"}
								alt="密码财经微博"
								style={{ width: 150, height: 150 }}
							/>
						</Card>
					</Col>
				</Row>
				<Row
					type="flex"
					align="start"
					style={{ minWidth: 1200, alignSelf: "center" }}
				>
					<Card title="友情链接" bordered={false} bodyStyle={{ padding: 1 }}>
						<ul>
							<li>
								<a
									target="_blank"
									rel="noopener noreferrer"
									href="https://bitpie.com/"
								>
									比特派钱包
								</a>
							</li>
							<li>
								<a
									target="_blank"
									rel="noopener noreferrer"
									href="https://hoo.com/"
								>
									Hoo 虎符钱包
								</a>
							</li>
							<li>
								<a
									target="_blank"
									rel="noopener noreferrer"
									href="https://www.qiedalu.com/"
								>
									企鹅大陆
								</a>
							</li>
							<li>
								<a
									target="_blank"
									rel="noopener noreferrer"
									href="https://coindaily.io/"
								>
									每日币读
								</a>
							</li>
							<li>
								<a
									target="_blank"
									rel="noopener noreferrer"
									href="https://assetfun.org/"
								>
									Assetfun
								</a>
							</li>
							<li>
								<a
									target="_blank"
									rel="noopener noreferrer"
									href="https://www.xicaijing.com/"
								>
									烯财经
								</a>
							</li>
							<li>
								<a
									target="_blank"
									rel="noopener noreferrer"
									href="https://www.8btc.com/"
								>
									巴比特
								</a>
							</li>
						</ul>
					</Card>
				</Row>
			</Col>

			<hr style={{ border: "1px solid #cccccc" }} />
			<Col style={{ display: "flex", justifyContent: "center" }}>
				<Row type="flex" justify="center" style={{ width: 1200 }}>
					<Card style={{ fontSize: 14, color: "#666666" }} bordered={false}>
						Copyright 密码财经 版权所有 未经许可不得转载
						<Divider type="vertical" />粤 ICP 备 18141619 号
					</Card>
				</Row>
			</Col>
		</Row>
	);
};

export default Footer;
