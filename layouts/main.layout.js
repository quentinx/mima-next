import { Layout } from "antd";
import { Component } from "react";
import { connect } from "react-redux";
import Footer from "./Footer";
import Header from "./Header";
class MainLayout extends Component {
	render() {
		return (
			<Layout className="layout">
				<Layout.Header>
					<Header />
				</Layout.Header>
				<Layout.Content
					style={{
						width: 1200
					}}
				>
					{this.props.children}
				</Layout.Content>
				<Layout.Footer>
					<Footer />
				</Layout.Footer>
			</Layout>
		);
	}
}

const mapStateToProps = state => {
	return {
		auth: state.auth
	};
};

export default connect(
	mapStateToProps,
	{}
)(MainLayout);
