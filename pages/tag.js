import { Button, Divider, Row, Col, Tag } from "antd";
import React, { Fragment } from "react";
import { authorListViewWrapper } from "utils/wrapper";
import { getTag, getMoreTag, getLatestFive } from "../actions";
import MimaList from "components/MimaList";
import MimaCard from "components/MimaCard";
import QuickNews from "components/QuickNews";

class Tags extends React.Component {
	static async getInitialProps(ctx) {
		// do something in server here!
		const { store, query } = ctx;

		const { tag } = query;
		await store.dispatch(getTag(tag));
		return store.getState();
	}

	state = {
		page: 1
	};

	onLoadMore = () => {
		this.props.loadMore(decodeURI(this.props.url.query.tag), ++this.state.page);
	};
	render() {
		const loadMore = !this.props.loading ? (
			this.props.pages === this.state.page ? (
				<div
					style={{
						textAlign: "center",
						marginTop: 12,
						height: 32,
						lineHeight: "32px"
					}}
				>
					<Divider>🎉</Divider>
				</div>
			) : (
				<div
					style={{
						textAlign: "center",
						marginTop: 12,
						height: 32,
						lineHeight: "32px"
					}}
				>
					<Button onClick={this.onLoadMore}>浏览更多</Button>
				</div>
			)
		) : (
			<div
				style={{
					textAlign: "center",
					marginTop: 12,
					height: 32,
					lineHeight: "32px"
				}}
			>
				<Button onClick={this.onLoadMore}>加载中...</Button>
			</div>
		);

		return (
			<Row type="flex" justify="center">
				<Row type="flex" gutter={10} style={{ width: 1200 }}>
					<Col style={{ width: 820 }}>
						<MimaCard
							title={
								<Row type="flex">
									<Row>标签：</Row>

									<Row type="flex" align="middle">
										<Tag color="#ff9b2b">
											{decodeURI(this.props.url.query.tag)}
										</Tag>
									</Row>
								</Row>
							}
						>
							<MimaList
								dataSource={this.props.dataSource}
								loadMore={loadMore}
								loading={this.props.loading}
							/>
						</MimaCard>
					</Col>
				</Row>
			</Row>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		loading: state.post.loading,
		dataSource: state.post.tag.data.posts,
		pages: state.post.tag.data.pages
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		loadMore: (tag, page) => dispatch(getMoreTag(tag, page))
	};
};

export default authorListViewWrapper(mapStateToProps, mapDispatchToProps)(Tags);
