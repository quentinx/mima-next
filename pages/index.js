import { Button, Carousel, Col, Divider, Row } from "antd";
import CategoryHeader from "components/CategoryHeader";
import MimaCard from "components/MimaCard";
import MimaList from "components/MimaList";
import Weibo from "components/Weibo";
import QuickNews from "components/QuickNews";
import MimaVideo from "components/MimaVideo";
import TagsCloud from "components/TagsCloud";

import React from "react";
import { blogListViewWrapper } from "utils/wrapper";
import {
	getCategories,
	getCategoryPosts,
	getLatestFive,
	getMoreCategoryPosts,
	getMimaVideo,
	getMimaVideo2,
	getCarousel,
	getTags,
	getPostsByCategory
} from "../actions";

class Index extends React.Component {
	static async getInitialProps(ctx) {
		// do something in server here!
		const { store } = ctx;

		await store.dispatch(getLatestFive());
		await store.dispatch(getCategories());
		await store.dispatch(getCarousel());
		await store.dispatch(getMimaVideo());
		await store.dispatch(getMimaVideo2());
		await store.dispatch(getPostsByCategory("DApp"));
		await store.dispatch(getTags());
		await store.dispatch(getCategoryPosts("资讯", 1, true));
		return store.getState();
	}

	state = {
		page: 1
	};

	onLoadMore = () => {
		this.props.loadMore(++this.state.page);
	};

	render() {
		const parentCategory = this.props.post.categories.data.categories.find(
			(x) => x.title === "首页"
		);
		const id = parentCategory.id;
		const categories = this.props.post.categories.data.categories.filter(
			(x) => x.parent === id
		);
		const location = this.props.url;

		const loadMore = !this.props.loading ? (
			this.props.pages === this.state.page ? (
				<div
					style={{
						textAlign: "center",
						marginTop: 12,
						height: 32,
						lineHeight: "32px"
					}}
				>
					<Divider>🎉</Divider>
				</div>
			) : (
				<div
					style={{
						textAlign: "center",
						marginTop: 12,
						height: 32,
						lineHeight: "32px"
					}}
				>
					<Button onClick={this.onLoadMore}>浏览更多</Button>
				</div>
			)
		) : null;

		return (
			<Row span={24} gutter={10}>
				<Col span={16}>
					<Carousel autoplay>
						{this.props.post.carousel.data.map((x) => (
							<div dangerouslySetInnerHTML={{ __html: x }} />
						))}
					</Carousel>
					<MimaCard
						style={{ marginTop: 10 }}
						title={
							<CategoryHeader
								categories={categories}
								parentCategory={parentCategory.title}
								location={location}
							/>
						}
					>
						<MimaList
							dataSource={this.props.dataSource}
							loadMore={loadMore}
							loading={this.props.loading}
						/>
					</MimaCard>
				</Col>
				<Col span={8}>
					<QuickNews posts={this.props.post.latestFive.data.posts} />
					<MimaCard title="DApp" style={{ marginBottom: 10, marginTop: 10 }}>
						<MimaVideo videos={this.props.post.postsByCategory.data.posts} />
					</MimaCard>
					<MimaCard title="密码专访" style={{ marginBottom: 10 }}>
						<MimaVideo videos={this.props.post.zhuanfang.data.posts} />
					</MimaCard>
					<MimaCard title="密码情报站" style={{ marginBottom: 10 }}>
						<MimaVideo videos={this.props.post.qingbaozhan.data.posts} />
					</MimaCard>
					<Weibo />
					<TagsCloud data={this.props.tags} />
				</Col>
			</Row>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		loading: state.post.loading,
		dataSource: state.post.category.data.posts,
		pages: state.post.category.data.pages,
		tags: state.post.tags
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		loadMore: (page) => dispatch(getMoreCategoryPosts("资讯", page))
	};
};

export default blogListViewWrapper(mapStateToProps, mapDispatchToProps)(Index);
