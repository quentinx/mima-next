import React, { Fragment } from "react";
import { blogListViewWrapper } from "utils/wrapper";

class About extends React.PureComponent {
	render() {
		return (
			<Fragment>
				{/* <Helmet>
                <meta charset="utf-8" />
                <title>公司简介</title>
            </Helmet> */}
				<div style={{ width: 1200, margin: "0 auto" }}>
					<h2 className={"bt01"}>
						<a>关于密码财经</a>
						<span>揭开数字币投资的秘密</span>
					</h2>
					<div style={{ clear: "both" }} />
					<div className={"mmcj"}>
						<dl>
							<dd>
								<h2>
									Focus on
									<em>密码财经——专注区块链和数字币新闻素材深度挖掘的媒体</em>
								</h2>
								<p>
									密码财经是一家专注区块链和数字币新闻素材深度挖掘的媒体，子品牌有币升客、牌友投资圈、币定投、比特人社区、比特币永远涨，覆盖到微博、微信、头条等新媒体平台。
								</p>
								<p>
									原创的视频栏目《密码情报站》，挖掘最新鲜最有料的区块链和数字币的新鲜素材；《密码财经周刊》，捕捉区块链行业动向、新鲜资讯与热点话题；《密码专访》，锁定区块链行业前沿公司和话题人物。
								</p>
							</dd>
							<dt>
								<img
									alt="密码财经"
									src={"/static/image/about.jpg"}
									title="密码财经"
									style={{ align: "center" }}
								/>
							</dt>
						</dl>
					</div>
					<div className={"clear"} />

					<h2 className={"bt01"}>
						<a>联系密码财经</a>
						<span>揭开数字币投资的秘密</span>
					</h2>

					<div style={{ clear: "both" }} />

					<div className={"lx"}>
						<p>
							<b>邮箱：</b>
							bd@mimacaijing.com
						</p>
					</div>

					<div className={"ewm"}>
						<dd>
							<b>二维码：</b>
						</dd>

						<dt>
							<img
								src={"/static/image/about_wechat.jpg"}
								alt="密码财经二维码"
								style={{
									width: 150,
									height: 150
								}}
							/>
						</dt>
					</div>

					<div style={{ clear: "both" }} />
				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		loading: state.post.loading
	};
};

export default blogListViewWrapper(mapStateToProps, {})(About);
