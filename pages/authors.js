import React from "react";
import { Row, Col } from "antd";
import { authorListViewWrapper } from "utils/wrapper";
import { getAuthors, setMetaCategories } from "../actions";
import AuthorCard from "components/AuthorCard";
import AuthorHeader from "components/AuthorHeader";

class AuthorList extends React.Component {
	static async getInitialProps(ctx) {
		// do something in server here!
		const { store } = ctx;

		await store.dispatch(getAuthors());
		await store.dispatch(setMetaCategories(6));
		return store.getState();
	}

	render() {
		return (
			<React.Fragment>
				<AuthorHeader count={this.props.post.authors.length} />

				<Row
					type="flex"
					justify="space-between"
					gutter={20}
					style={{ width: 1200, margin: "0 auto" }}
				>
					{this.props.post.authors.map(x => {
						const { author, count, pageviews } = x;
						const { description, id, name, slug, url } = author;
						return (
							<Col
								style={{ width: "33.3%", height: "auto", marginBottom: 20 }}
								key={id}
							>
								<AuthorCard
									author={{ id, pageviews, count, url, name, description }}
								/>
							</Col>
						);
					})}
				</Row>
			</React.Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		// auth: state.auth
		// latestFive: state.latestFive
	};
};

// export default connect(({ latestFive }) => ({ latestFive }))(AuthorList);

export default authorListViewWrapper(mapStateToProps, {})(AuthorList);
