import { Avatar, Card, Col, Divider, Row, Tag } from "antd";
import md5 from "md5";
import Link from "next/link";
import React from "react";
import { blogViewWrapper } from "utils/wrapper";
import { getBlog, getLatestFive } from "../actions";
import BlogCard from "/components/BlogCard";
import PreviousNextPosts from "/components/PreviousNextPosts";
import QuickNews from "/components/QuickNews";
import Related from "/components/Related";
import Weibo from "components/Weibo";

class Blog extends React.Component {
	static async getInitialProps(ctx) {
		// do something in server here!
		const { store, query } = ctx;
		const { blogId } = query;

		await store.dispatch(getBlog(blogId));
		return store.getState();
	}
	render() {
		const { post: blog, next, previous, related } = this.props.post.blog.data;
		const { author: authorInfo } = blog;

		return (
			<Row type="flex" justify="center" className="blog">
				<Col>
					<Row type="flex" justify="space-between" gutter={20}>
						<BlogCard
							headStyle={{ height: "auto" }}
							title={
								<Row>
									<Row
										style={{
											fontSize: 24,
											whiteSpace: "pre-line",
											maxWidth: 772
										}}
									>
										{blog.title}
									</Row>
									<Row
										type="flex"
										justify="space-between"
										style={{
											fontSize: 14,
											color: "#737373",
											marginTop: 12,
											width: 700
										}}
									>
										{authorInfo.name && (
											<Col>
												<img
													src={`https://www.gravatar.com/avatar/${md5(
														authorInfo.url
															.replace("http://", "")
															.toLowerCase()
															.trim()
													)}`}
													alt="avatar"
													style={{ width: 20, height: 20 }}
												/>
												<span style={{ marginLeft: 5 }}>
													<Link href={`/@${authorInfo.name}`}>
														<a target="_blank">{authorInfo.name}</a>
													</Link>
												</span>
											</Col>
										)}
										{blog.modified && <Col>{`发布时间: ${blog.modified}`}</Col>}
										{blog.tags && (
											<Col>
												关键字：
												{blog.tags.map((x) => (
													<Tag color="#ff9b2b">
														<a href={`/tag/${x.title}`} target="_blank">
															{x.title}
														</a>
													</Tag>
												))}
											</Col>
										)}
									</Row>
								</Row>
							}
							style={{ width: 820, paddingLeft: 0, paddingRight: 0 }}
						>
							<div
								style={{
									lineHeight: "26px",
									fontSize: 16
								}}
								dangerouslySetInnerHTML={{ __html: blog.content }}
							/>
							<Divider />
							<Related
								related={related}
								style={{ marginTop: 40, marginBottom: 86 }}
							/>
							<PreviousNextPosts
								previous={previous}
								next={next}
								style={{ marginTop: 86, marginBottom: 80 }}
							/>
						</BlogCard>
						<Col style={{ width: 360 }}>
							<Card bordered={false}>
								<Card.Meta
									avatar={
										<Link href={`/@${authorInfo.name}`}>
											<a target="_blank">
												<Avatar
													src={`https://www.gravatar.com/avatar/${md5(
														authorInfo.url
															.replace("http://", "")
															.toLowerCase()
															.trim()
													)}`}
												/>
											</a>
										</Link>
									}
									title={
										<Link href={`/@${authorInfo.name}`}>
											<a target="_blank">{authorInfo.name}</a>
										</Link>
									}
									description={authorInfo.description}
								/>
							</Card>
							<div style={{ marginTop: 10 }}>
								<Weibo />
							</div>
						</Col>
					</Row>
				</Col>
			</Row>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		// auth: state.auth
		// latestFive: state.latestFive
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		// getRelated: categoryId => dispatch(getRelated(categoryId)),
		// getPreviousNext: blogId => dispatch(getPreviousNext(blogId))
	};
};

export default blogViewWrapper(mapStateToProps, mapDispatchToProps)(Blog);
