import { Button, Carousel, Col, Divider, Row } from "antd";
import CategoryHeader from "components/CategoryHeader";
import MimaCard from "components/MimaCard";
import MimaList from "components/MimaList";
import QuickNews from "components/QuickNews";
import Weibo from "components/Weibo";
import React from "react";
import { blogListViewWrapper } from "utils/wrapper";
import {
	getCategories,
	getCategoryPosts,
	getLatestFive,
	getMoreCategoryPosts,
	getCarousel
} from "../actions";

class Blogs extends React.Component {
	static async getInitialProps(ctx) {
		// do something in server here!
		const { store, query } = ctx;
		const { category, page } = query;

		// await store.dispatch(getLatestFive());
		await store.dispatch(getCarousel());
		await store.dispatch(getCategories());
		await store.dispatch(getCategoryPosts(category, 1));
		return store.getState();
	}

	state = {
		page: 1
	};

	onLoadMore = () => {
		this.props.loadMore(
			decodeURI(this.props.url.query.category),
			++this.state.page
		);
	};
	render() {
		const parentCategory = this.props.post.categories.data.categories.find(
			(x) => x.title === decodeURI(this.props.url.query.category)
		);
		const id = parentCategory.id;
		const categories = this.props.post.categories.data.categories.filter(
			(x) => x.parent === id
		);
		const location = this.props.url;

		const loadMore = !this.props.loading ? (
			this.props.pages === this.state.page ? (
				<div
					style={{
						textAlign: "center",
						marginTop: 12,
						height: 32,
						lineHeight: "32px"
					}}
				>
					<Divider>🎉</Divider>
				</div>
			) : (
				<div
					style={{
						textAlign: "center",
						marginTop: 12,
						height: 32,
						lineHeight: "32px"
					}}
				>
					<Button onClick={this.onLoadMore}>浏览更多</Button>
				</div>
			)
		) : (
			<div
				style={{
					textAlign: "center",
					marginTop: 12,
					height: 32,
					lineHeight: "32px"
				}}
			>
				<Button onClick={this.onLoadMore}>加载中...</Button>
			</div>
		);

		return (
			<Row span={24} gutter={10}>
				<Col span={16}>
					{/* <Carousel autoplay>
						{this.props.post.carousel.data.map(x => <div dangerouslySetInnerHTML={{ __html: x }} />)}
					</Carousel> */}
					<MimaCard
						style={{ marginTop: 10 }}
						title={
							categories.length > 0 ?
							<CategoryHeader
								categories={categories}
								parentCategory={parentCategory.title}
								location={location}
								/> : parentCategory.title
						}
					>
						<MimaList
							dataSource={this.props.dataSource}
							loadMore={loadMore}
							loading={this.props.loading}
						/>
					</MimaCard>
				</Col>
				<Col span={8}>
					<Weibo />
				</Col>
			</Row>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		loading: state.post.loading,
		dataSource: state.post.category.data.posts,
		pages: state.post.category.data.pages
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		loadMore: (category, page) => dispatch(getMoreCategoryPosts(category, page))
	};
};

export default blogListViewWrapper(mapStateToProps, mapDispatchToProps)(Blogs);
