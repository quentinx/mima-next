import { Table, Row } from "antd";
import React from "react";
import { blogListViewWrapper } from "utils/wrapper";
import { getDataSource, setMetaCategories } from "../actions";

class Market extends React.Component {
	static async getInitialProps(ctx) {
		const { store } = ctx;
		await store.dispatch(getDataSource());
		await store.dispatch(setMetaCategories(176));
		return store.getState();
	}

	render() {
		const dataSource = this.props.dataSource;
		const columns = [
			{
				key: "id",
				dataIndex: "id",
				title: "排名",
				render: (text, record, index) => `No.${index + 1}`
			},
			{
				key: "symbol",
				dataIndex: "symbol",
				title: "币种",
				render: (text, record) => {
					return (
						<Row type="flex" align="middle">
							<img
								width={15}
								src={`http://app.mimacaijing.net/image/logo/${record.id}/`}
							/>
							<span style={{ marginLeft: 5 }}>{text}</span>
						</Row>
					);
				}
			},
			{
				key: "circulating_supply",
				dataIndex: "circulating_supply",
				title: "流通供给量",
				render: (text, record) => (
					<span>
						<span style={{ color: "#0E9B97" }}>{text}</span>
						{` ${record.symbol}`}
					</span>
				)
			},
			{
				key: "market_cap",
				dataIndex: "market_cap",
				title: "市值",
				render: (text) => <span>{`$${text}`}</span>
			},
			{
				key: "price",
				dataIndex: "price",
				title: "价格",
				render: (text) => (
					<span style={{ color: "#0E9B97" }}>{`$${parseFloat(text).toFixed(
						3
					)}`}</span>
				)
			},
			{
				key: "volume_24h",
				dataIndex: "volume_24h",
				title: "交易量(24小时)",
				render: (text) => <span style={{ color: "#FF9F2E" }}>{`$${text}`}</span>
			},
			{
				key: "percent_change_24h",
				dataIndex: "percent_change_24h",
				title: "变化量(24小时)",
				render: (text) =>
					parseFloat(text) >= 0 ? (
						<span style={{ color: "#F06646" }}>{`+${text}%`}</span>
					) : (
						<span style={{ color: "#20B435" }}>{`${text}%`}</span>
					)
			},
			{
				key: "image",
				dataIndex: "image",
				title: "价格走势图(7天)",
				render: (text, record) => {
					return (
						<img
							width={150}
							src={`http://app.mimacaijing.net/image/${record.id}/`}
						/>
					);
				}
			}
		];
		return (
			<Table
				dataSource={dataSource}
				columns={columns}
				className={"market_table"}
				pagination={false}
				rowClassName={(record, index) =>
					index % 2 === 0 ? "row_odd" : "row_even"
				}
			/>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dataSource: state.post.dataSource.data
	};
};

export default blogListViewWrapper(mapStateToProps, {})(Market);
