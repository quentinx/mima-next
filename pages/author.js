import { Col, Row } from "antd";
import AuthorDetailHeader from "components/AuthorDetailHeader";
import MimaCard from "components/MimaCard";
import MimaList from "components/MimaList";
import React, { Fragment } from "react";
import { authorViewWrapper } from "utils/wrapper";
import { getAuthor } from "../actions";

class Author extends React.Component {
	static async getInitialProps(ctx) {
		// do something in server here!
		const { store, query } = ctx;

		await store.dispatch(getAuthor(query.authorName));

		return store.getState();
	}

	render() {
		const {
			url,
			name,
			description,
			count,
			pageviews,
			posts
		} = this.props.post.author;
		return (
			<Fragment>
				<AuthorDetailHeader
					author={{ url, name, description, count, pageviews }}
				/>
				<Row type="flex" justify="center">
					<Row type="flex" gutter={20} style={{ width: 1200 }}>
						<Col style={{ width: 820 }}>
							<MimaCard title="文章列表">
								<MimaList dataSource={posts} />
							</MimaCard>
						</Col>
						<Col style={{ width: 360 }}>
							<MimaCard title="个人简介">{description}</MimaCard>
						</Col>
					</Row>
				</Row>
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		// auth: state.auth
		// latestFive: state.latestFive
	};
};

// export default connect(({ latestFive }) => ({ latestFive }))(Author);

export default authorViewWrapper(mapStateToProps, {})(Author);
