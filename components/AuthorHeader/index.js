import { Col, Row } from "antd";
import React from "react";
import MimaCard from "../MimaCard";

const AuthorHeader = ({ count, loading }) => {
	return (
		<Row className={"AuthorHeader"} type="flex" justify="center">
			<Col style={{ width: 1200 }}>
				<Row style={{ marginBottom: 22 }}>专栏 / 专栏作者</Row>
				<Row style={{ height: 70 }}>
					<MimaCard>
						<Row type="flex" align="middle" justify="space-between">
							<Col style={{ fontSize: 20, fontWeight: "bold" }}>
								密码财经共有{" "}
								<span style={{ color: "#ff9b2b", fontSize: 26 }}>{count}</span>{" "}
								位专栏作者
							</Col>
						</Row>
					</MimaCard>
				</Row>
			</Col>
		</Row>
	);
};
export default AuthorHeader;
