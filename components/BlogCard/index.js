import { Card } from "antd";
import React from "react";

const BlogCard = ({ title, headStyle, bodyStyle, children }) => {
	return (
		<Card
			title={title}
			headStyle={{ ...headStyle }}
			bodyStyle={{ ...bodyStyle }}
			className="blog_card"
			bordered={false}
		>
			{children}
		</Card>
	);
};

export default BlogCard;
