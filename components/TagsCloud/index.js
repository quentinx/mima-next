import { Badge, Tag } from "antd";
import MimaCard from "components/MimaCard";
import React, { Component } from "react";

class TagsCloud extends Component {
	render() {
		return (
			<MimaCard title="热门标签" style={{ marginTop: 10 }}>
				<div
					style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}
				>
					{this.props.data
						.sort(
							(a, b) =>
								(b.post_count > a.post_count) - (a.post_count > b.post_count)
						)
						.slice(0, 10)
						.map(x => (
							<div style={{ margin: 3 }} key={x.id}>
								<a target="_blank" href={`/tag/${x.title}`}>
									<Tag color="#ff9b2b">{x.title}</Tag>
								</a>
							</div>
						))}
				</div>
			</MimaCard>
		);
	}
}

export default TagsCloud;
