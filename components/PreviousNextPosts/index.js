import { Col, Row } from "antd";
import React from "react";
import MimaLink from "components/MimaLink";

const PreviousNextPosts = ({ previous, next, style }) => {
	return (
		<Row className="previous_next_posts" style={{ ...style }}>
			<Row style={{ marginBottom: 18 }}>
				{next && (
					<Row type="flex" style={{ flexDirection: "row" }}>
						<Col span={2}>下一篇</Col>
						<Col>
							<MimaLink id={next.id} title={next.title} />
						</Col>
					</Row>
				)}
			</Row>
			<Row style={{ marginTop: 18 }}>
				{previous && (
					<Row type="flex" style={{ flexDirection: "row" }}>
						<Col span={2}>上一篇</Col>
						<Col>
							<MimaLink id={previous.id} title={previous.title} />
						</Col>
					</Row>
				)}
			</Row>
		</Row>
	);
};

export default PreviousNextPosts;
