import React from "react";
import { Col, List, Row } from "antd";
import Link from "next/link";
import MimaLink from "../MimaLink";

class MimaList extends React.PureComponent {
	render() {
		const { dataSource, loading, loadMore } = this.props;

		return (
			<List
				className={"mima_list"}
				itemLayout="vertical"
				dataSource={dataSource}
				loading={loading}
				loadMore={loadMore}
				renderItem={item => (
					<Row type="flex" style={{ padding: 24 }} className={"tab"}>
						<Col style={{ width: "40%", height: "auto" }}>
							<img
								src={item.thumbnail}
								width={264}
								height={165}
								alt="thumbnail"
							/>
						</Col>
						<Col style={{ width: "60%", height: "auto" }}>
							<List.Item
								key={item.title}
								actions={[
									<div>
										<Link href={`/@${item.author.name}`}>
											<a target="_blank">{item.author.name}</a>
										</Link>
									</div>,
									<div>
										<Link href={`/p/${item.id}`}>
											<a target="_blank">{item.date}</a>
										</Link>
									</div>
								]}
							>
								<List.Item.Meta
									title={<MimaLink id={item.id} title={item.title} style={{color: 'black'}} />}
								/>
								<div dangerouslySetInnerHTML={{ __html: item.excerpt }} />
							</List.Item>
						</Col>
					</Row>
				)}
			/>
		);
	}
}

export default MimaList;
