import { Card } from "antd";
import React from "react";

const MimaCard = ({
	keyValue,
	headStyle,
	bodyStyle,
	title,
	children,
	style
}) => (
	<Card
		key={keyValue}
		className={"mima_card"}
		bordered={false}
		style={{ ...style }}
		headStyle={{ ...headStyle, height: 70, lineHeight: "40px" }}
		bodyStyle={{ ...bodyStyle }}
		title={title}
	>
		{children}
	</Card>
);

export default MimaCard;
