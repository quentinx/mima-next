import React from "react";
import { connect } from "react-redux";
import Link from "next/link";

const MimaLink = ({ id, title, dispatch, style }) => {
	const count = id => {
		dispatch({
			type: "blog/setPageview",
			payload: id
		});
	};

	return (
		<Link href={`/p?id=${id}`} as={`/p/${id}`}>
			<a target="_blank" className={"mima_link"} style={{ ...style }}>
				{title}
			</a>
		</Link>
	);
};

export default connect(({ dispatch }) => ({ dispatch }))(MimaLink);
