import Link from "next/link";
import { Menu } from "antd";
import React from "react";
import arrayToTree from "utils/arrayToTree";
const styles = {
	menu: {
		fontWeight: 600,
		borderBottom: "none",
		fontSize: 20
	}
};

const CategoryHeader = ({ categories, parentCategory, location }) => {
	const menuTree = arrayToTree(categories, "id", "parent");
	const levelMap = {};
	// 递归生成菜单
	const getMenus = (menuTreeN, siderFoldN) => {
		return menuTreeN.map(item => {
			if (item.children) {
				if (item.mpid) {
					levelMap[item.id] = item.mpid;
				}
				return (
					<Menu.SubMenu
						key={item.title}
						title={
							<span>
								{(!siderFoldN || !menuTree.includes(item)) && item.title}
							</span>
						}
					>
						{getMenus(item.children, siderFoldN)}
					</Menu.SubMenu>
				);
			}
			return (
				<Menu.Item key={item.title}>
					{(!siderFoldN || !menuTree.includes(item)) && (
						<a href={`/s/${parentCategory}/${item.title}`} style={{color: 'black'}}>{item.title}</a>
					)}
				</Menu.Item>
			);
		});
	};

	const menuItems = getMenus(menuTree, false);
	const slug = location.query.category
		? decodeURI(location.query.category)
		: "资讯";
	return (
		<Menu style={styles.menu} selectedKeys={[slug]} mode="horizontal">
			{menuItems}
		</Menu>
	);
};

export default CategoryHeader;
