import { Avatar, Card } from "antd";
import md5 from "md5";
import React from "react";
import Link from "next/link";

const AuthorCard = ({ title, headStyle, bodyStyle, author }) => {
	const { id, pageviews, count, url, name, description } = author;
	return (
		<Card
			title={title}
			headStyle={{ ...headStyle }}
			bodyStyle={{ ...bodyStyle }}
			className={"author_card"}
			bordered={false}
			key={id}
			actions={[
				pageviews && (
					<div style={{ display: "flex", flexDirection: "column" }}>
						<span>浏览量</span>
						<span>{pageviews}</span>
					</div>
				),
				count && (
					<Link href={`/author/${id}`} as={`/@${encodeURIComponent(name)}`}>
						<a target="_blank">
							<div style={{ display: "flex", flexDirection: "column" }}>
								<span>文章数量</span>
								<span>{count}</span>
							</div>
						</a>
					</Link>
				)
			]}
		>
			<Card.Meta
				avatar={
					<Avatar
						src={`https://www.gravatar.com/avatar/${md5(
							url
								.replace("http://", "")
								.toLowerCase()
								.trim()
						)}`}
					/>
				}
				title={
					<Link href={`/author/${id}`} as={`/@${encodeURIComponent(name)}`}>
						<a target="_blank">{name}</a>
					</Link>
				}
				description={description}
			/>
		</Card>
	);
};

export default AuthorCard;
