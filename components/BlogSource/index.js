import React from "react";
import MimaCard from "../MimaCard";
import MimaList from "../MimaList";

const BlogSource = ({ dataSource, pages, keyValue, title, loading }) => {
  return (
    <MimaCard title={title} keyValue={keyValue} bodyStyle={{ padding: 0 }}>
      <MimaList
        dataSource={dataSource}
        pages={pages}
        loading={loading.global}
      />
    </MimaCard>
  );
};
export default BlogSource;
