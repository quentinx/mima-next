import { Row, Timeline } from "antd";
import React from "react";
import MimaLink from "../MimaLink";
import MimaCard from "../MimaCard";

const QuickNews = ({ posts }) => {
	return (
		<MimaCard title="24 小时快讯">
			<Timeline>
				{posts.map(x => (
					<Timeline.Item key={x.id}>
						<Row>{x.date}</Row>
						<Row
							style={{
								marginTop: 12,
								marginBottom: 12,
								fontSize: 16,
								fontWeight: "bold",
								color: "#404040"
							}}
						>
							<MimaLink id={x.id} title={x.title} />
						</Row>
						<div
							style={{ fontSize: 14, color: "#999999" }}
							dangerouslySetInnerHTML={{ __html: x.excerpt }}
						/>
					</Timeline.Item>
				))}
			</Timeline>
		</MimaCard>
	);
};

export default QuickNews;
