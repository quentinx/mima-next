import { Avatar, Card, Row, Skeleton } from "antd";
import md5 from "md5";
import React from "react";

const AuthorDetailHeader = props => {
	const { url, name, description, count, pageviews } = props.author;
	return (
		<Row className={"AuthorDetailHeader"} type="flex" justify="center">
			<Row style={{ width: 1200 }}>
				<Card
					bordered={false}
					style={{ backgroundColor: "transparent" }}
					actions={[
						pageviews && (
							<div style={{ display: "flex", flexDirection: "column" }}>
								<span>浏览量</span>
								<span>{pageviews}</span>
							</div>
						),
						count && (
							<div style={{ display: "flex", flexDirection: "column" }}>
								<span>文章数量</span>
								<span>{count}</span>
							</div>
						)
					]}
				>
					<Card.Meta
						avatar={
							<Avatar
								src={`https://www.gravatar.com/avatar/${md5(
									url
										.replace("http://", "")
										.toLowerCase()
										.trim()
								)}`}
							/>
						}
						title={name}
						description={description}
					/>
				</Card>
			</Row>
		</Row>
	);
};
export default AuthorDetailHeader;
