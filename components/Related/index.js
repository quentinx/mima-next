import { Row, Col } from "antd";
import React from "react";
import MimaLink from "components/MimaLink";

const Related = ({ related, style }) => {
	return (
		related.length > 0 && (
			<Row className="related" style={{ ...style }}>
				<Row style={{ color: "#737373", marginBottom: 26 }}>相关推荐：</Row>
				<Row type="flex">
					{related.map((r) => (
						<Col style={{ width: "33.3%" }} key={r.id}>
							<Row>
								<img src={r.thumbnail} alt="thumbnail" width={"90%"} />
							</Row>
							<Row
								style={{
									padding: "10px 24px",
									lineHeight: "20px",
									height: 50,
									overflow: "hidden"
								}}
							>
								<MimaLink
									id={r.id}
									title={
										<div
											style={{
												overflow: "hidden",
												textOverflow: "ellipsis",
												display: "-webkit-box",
												webkitLineClamp: "2",
												WebkitBoxOrient: "vertical"
											}}
										>
											{r.title}
										</div>
									}
								/>
							</Row>
							<Row
								style={{
									padding: "15px 19px",
									fontSize: "0.8125rem",
									color: "#888"
								}}
							>
								{r.date}
							</Row>
						</Col>
					))}
				</Row>
			</Row>
		)
	);
};

export default Related;
