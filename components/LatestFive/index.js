import { Component } from "react";
import { Modal, Input, Form, Button, Checkbox, Icon, List } from "antd";
import { connect } from "react-redux";

// import { authControlLatestFive, authLogin } from "actions"
import { getLatestFive } from "../../actions";

class LatestFive extends Component {
	render() {
		return <List>{this.props.children}</List>;
	}
}

const mapStateToProps = state => {
	return {
		// LatestFiveVisible: state.auth.LatestFiveVisible
	};
};

export default connect(
	mapStateToProps,
	{}
)(LatestFive);
