import React, { PureComponent, Fragment } from "react";
import { Carousel, Row, Col } from "antd";
import { connect } from "react-redux";
import Link from "next/link";

class MimaVideo extends PureComponent {
	render() {
		return (
			<Row className={"mima_video"}>
				<Carousel autoplay>
					{this.props.videos.map((x) => (
						<div key={x.id}>
							<Link href={`/p?id=${x.id}`} as={`/p/${x.id}`}>
								<a target="_blank">
									<Row>
										<Row type="flex" justify="center">
											<img
												src={x.thumbnail_images.medium.url}
												alt="video"
												height={200}
											/>
										</Row>
										<Row type="flex" justify="center" style={{ marginTop: 5 }}>
											{x.title}
										</Row>
									</Row>
								</a>
							</Link>
						</div>
					))}
				</Carousel>
			</Row>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		// videos: state.post.videos.data.posts
	};
};

export default connect(
	mapStateToProps,
	{}
)(MimaVideo);
