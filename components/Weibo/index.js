import React from "react";
import MimaCard from "components/MimaCard";

const Weibo = props => (
	<MimaCard
		title="密码财经微博"
		style={{ marginTop: 10 }}
		bodyStyle={{ padding: 0 }}
	>
		<iframe
			id="weibo"
			style={{ width: "100%", height: 500, overflowY: "scroll" }}
			frameBorder="0"
			scrolling="no"
			src="//v.t.sina.com.cn/widget/widget_blog.php?uid=5955571468"
		/>
	</MimaCard>
);

export default Weibo;