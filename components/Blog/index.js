import { Avatar, Card, Col, Row, Divider } from "antd";
import { connect } from "react-redux";
import React from "react";
import md5 from "md5";
import Related from "./Related";
import PreviousNextPosts from "./PreviousNextPosts";
import BlogCard from "./BlogCard";
import QuickNews from "./QuickNews";

const Blog = ({ blog }) => {
	const authorInfo = blog.author;
	const { news } = app;
	return (
		<Row type="flex" justify="center" className="blog">
			<Col>
				<Row type="flex" justify="space-between" gutter={20}>
					<BlogCard
						headStyle={{ height: 110 }}
						title={
							<Row>
								<Row style={{ fontSize: 24 }}>{blog.title}</Row>
								<Row
									type="flex"
									justify="space-between"
									style={{
										fontSize: 14,
										color: "#737373",
										marginTop: 12,
										width: 700
									}}
								>
									{authorInfo.name && (
										<Col>
											<img
												src={`https://www.gravatar.com/avatar/${md5(
													authorInfo.url
														.replace("http://", "")
														.toLowerCase()
														.trim()
												)}`}
												alt="avatar"
												style={{ width: 20, height: 20 }}
											/>
											{authorInfo.name}
										</Col>
									)}
									{blog.modified && <Col>{`发布时间: ${blog.modified}`}</Col>}
									{blog.tags && (
										<Col>{`关键字： ${blog.tags.map(
											(x) => `${x.title}`
										)}`}</Col>
									)}
								</Row>
							</Row>
						}
						style={{ width: 820, paddingLeft: 0, paddingRight: 0 }}
					>
						<div
							style={{
								lineHeight: "26px",
								fontSize: 16
							}}
							dangerouslySetInnerHTML={{ __html: blog.content }}
						/>
						<Divider />
						<Related
							related={blog.related}
							style={{ marginTop: 40, marginBottom: 86 }}
						/>
						<PreviousNextPosts
							blog={blog}
							style={{ marginTop: 86, marginBottom: 80 }}
						/>
					</BlogCard>
					<Col style={{ width: 360 }}>
						<Card bordered={false}>
							<Card.Meta
								avatar={
									<Avatar
										src={`https://www.gravatar.com/avatar/${md5(
											authorInfo.url
												.replace("http://", "")
												.toLowerCase()
												.trim()
										)}`}
									/>
								}
								title={authorInfo.name}
								description={authorInfo.description}
							/>
						</Card>
					</Col>
				</Row>
			</Col>
		</Row>
	);
};

export default connect(({ blog }) => ({ blog }))(Blog);
