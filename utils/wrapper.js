import connect from "store";
import EmptyViewContainer from "../hocs/BasicContainer";
import BlogListViewContainer from "../hocs/BlogListViewContainer";
import BlogViewContainer from "../hocs/BlogViewContainer";
import AuthorViewContainer from "../hocs/AuthorViewContainer";
import AuthorListViewContainer from "../hocs/AuthorListViewContainer";

export const emptyViewContainer = (mapStateToProps, mapDispatchToProps) => {
	return page =>
		connect(
			mapStateToProps,
			mapDispatchToProps
		)(EmptyViewContainer(page));
};

export const blogListViewWrapper = (mapStateToProps, mapDispatchToProps) => {
	return page =>
		connect(
			mapStateToProps,
			mapDispatchToProps
		)(BlogListViewContainer(page));
};

export const blogViewWrapper = (mapStateToProps, mapDispatchToProps) => {
	return page =>
		connect(
			mapStateToProps,
			mapDispatchToProps
		)(BlogViewContainer(page));
};
export const authorViewWrapper = (mapStateToProps, mapDispatchToProps) => {
	return page =>
		connect(
			mapStateToProps,
			mapDispatchToProps
		)(AuthorViewContainer(page));
};

export const authorListViewWrapper = (mapStateToProps, mapDispatchToProps) => {
	return page =>
		connect(
			mapStateToProps,
			mapDispatchToProps
		)(AuthorListViewContainer(page));
};
