import lodash from "lodash";
/**
 * 数组格式转树状结构
 * @param   {array}     array
 * @param   {String}    id
 * @param   {String}    parent
 * @param   {String}    children
 * @return  {Array}
 */
export default function arrayToTree(
	array,
	id = "id",
	parent = "parent",
	children = "children"
) {
	let data = lodash.cloneDeep(array);
	let result = [];
	let hash = {};
	data.forEach((item, index) => {
		hash[data[index][id]] = data[index];
	});

	data.forEach((item) => {
		let hashVP = hash[item[parent]];
		if (hashVP) {
			!hashVP[children] && (hashVP[children] = []);
			item = { ...item, parentSlug: hashVP["slug"] };
			hashVP[children].push(item);
		} else {
			result.push(item);
		}
	});
	return result;
}
